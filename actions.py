from engine import Interface, ai_interface, container, item

@Interface.register     
def look_at(interface, entity_id):
    """
    looks at or describes an object
    """
    pass

@Interface.register     
def look_at_content(interface, entity_id):
    """
    looks at or describes the contents of an object, like a container
    """
    pass
    
@Interface.register     
def open(interface, entity_id):
    """
    opens an object like a chest and a door
    """
    container.find(entity_id)["is_open"] = True
    
@Interface.register     
def close(interface, entity_id):
    """
    closes an object like a chest and a door
    """
    container.find(entity_id)["is_open"] = False
    
@Interface.register     
def put(interface, entity_id, container_id):
    """
    puts an object into a container or hand.
    """
    container_ = container.find(container_id)
    entity = item.find(entity_id)
    if not (container_[container]["is_open"] or container_[container]["is_always_open"]):
        return f"failure: {container_id} is closed"
    else:
        container << entity
        return "success"
   
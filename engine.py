from random import randrange
import operator
from functools import reduce
from interface import ai_interface
import inspect
import logging
from textwrap import fill
from random import choice

import logging.config
logging.config.dictConfig({
    'version': 1,
    # Other configs ...
    'disable_existing_loggers': True
})

class ComponentType:
    def __init__(self, classdef):
        self.definition = classdef
    def __call__(self, **kwargs):
        return Component(self, ** self.defaults | kwargs)
    def display(self, entity, config):
        q = self.definition.display(entity, **config)
        if q is None:
            return {}
        else:
            return dict(q)
    def find(self, id_):
        entity = Entity.all_[id_]
        assert self in entity, f"{entity} has no component {self.__name__}"
        return entity
    def __hash__(self):
        return hash(self.definition.__name__)


@ComponentType
class Core:
    visual = {}
    
    def core(entity, visual={}, type="entity"):
        yield ("visual", visual)
        yield ("type", type)
    
@ComponentType
def item(entity):
    pass

@ComponentType
def wearable(entity, slot=""):
    yield ("slot", slot)

@ComponentType
def container(entity, max_items=6, is_open=False, is_always_open=False, container_type=""):
    if not is_always_open:
        yield ("open", is_open)
    if container_type:
        yield ("container_type", container_type)
    if is_open or is_always_open:
        yield ("show_content", True)
        
@ComponentType
def hand(entity):
    yield ("show_content", True)
    
@ComponentType
def equipmentslot(entity, slot=""):
    yield ("body_part", slot)
    yield ("show_content", True)
    
@ComponentType
def location(entity):
    yield ("show_content", True)
    yield ("is_location", True)

    
class Component:
    def __init__(self, type_, **properties):
        self.type_ = type_
        self.properties = properties
        
    def __getitem__(self, key):
        return self.properties.__getitem__(key)
        
    def __contains__(self, key):
        return self.properties.__contains__(key)
    
    def __setitem__(self, key, value):
        return self.properties.__setitem__(key, value)
    
    def display(self, obj):
        return self.type_.display(obj, self.properties)
        
    def __repr__(self):
        return f"{self.type_}{self.properties}"

class Entity:
    all_ = {}
    contained = {}
    
    def __init__(self, *components, id_=..., ):
        self.components = {x.type_: x for x in components}
        logging.debug(f"created entity {self.components}")
        if id_ == ...:
            self.id_ = "${}_{:x}".format(self.components[core]["type"], randrange(16**4))
        else:
            self.id_ = id_
        self.all_[self.id_] = self
        
    def __getitem__(self, key):
        return self.components.__getitem__(key)
        
    def __contains__(self, key):
        return self.components.__contains__(key)
        
    def __lshift__(self, other):
        self.contained[other.id_] = self.id_
        
    def __str__(self):
        return str(self.local_descr())
        
    @property
    def parent(self):
        return self.all_.get(self.contained.get(self.id_, None), None)
    
    def children(self):
        yield from (self.all_.get(k) for k,v in self.contained.items() if v == self.id_)

    def local_descr(self):
        return {self.id_: reduce(operator.ior, [dict(x.display(self)) for x in self.components.values()], {})}
    
    @staticmethod
    def list_descr(lst):
        return reduce(operator.ior, [x.global_descr() for x in lst], {})
        
    def global_descr(self):
        loc = self.local_descr()
        loc_data = loc[self.id_]
        
        if "show_content" in loc_data:
            
            child_data = Entity.list_descr(self.children())
            loc_data["contents"] = list(child_data)
            
            loc |= child_data
            
            del loc_data["show_content"]
            
        return loc
        
def create_clothing(slot, name, material, color):
    return Entity(core(type=name, visual=dict(material=material, color=color)), item(), wearable(slot=slot))

def create_trinket(name, material, color):
    return Entity(core(type=name, visual=dict(material=material, color=color)), item())

def create_clothing_with_pockets(slot, name, material, color, pocket_refered_as="pockets"):
    return Entity(core(type=name, visual=dict(material=material, color=color)), item(), wearable(slot=slot), container(container_type=pocket_refered_as, max_items=2, is_always_open=True))     

def create_location(name):
    return Entity(core(type=name, visual=dict(walls=dict(color="grey", material="stone"), layout=name)), location())  
        
class Player():
    def __init__(self, startlocation):
        self.location = startlocation
        self.hands = {
            n:Entity(core(type="hand"), hand(), id_=f"${n}")
            for n in ["left_hand", "right_hand"]
            }
        self.equipment = {
            n:Entity(core(type="equipment-slot"), equipmentslot(slot=n), id_=f"${n}")
            for n in ["upper_body", "lower_body", "head", "feet", "back", "lower_arms", "hands"]
            }
        
        jacket = create_clothing_with_pockets("upper_body", "jacket", "leather", "green")
        backpack = create_clothing_with_pockets("back", "backpack", "leather", "blue", "backpack")
        
        self << jacket
        self << create_clothing("lower_body", "pants", "wool", "red")
        self << create_clothing("feet", "boots", "leather", "black")
        self << create_clothing("head", "hat", "leather", "green")
        self << backpack
        
        jacket << create_trinket("ring", "gold", "gold")
        
        
    def __lshift__(self, entity):
        slot = entity[wearable]["slot"]
        self.equipment[slot] << entity
            
        
        
        
        
        
class Interface:
    funcs = {}
    defs = {}
    def __init__(self, startlocation):
        self.player = Player(startlocation)
        
    def context(self):
        equipment = Entity.list_descr(self.player.equipment.values())
        hands = Entity.list_descr(self.player.hands.values())
        return self.player.location.global_descr() | \
            {"$player": {
                "equipment": list(equipment), 
                "hands": list(hands)
                }
             } | equipment | hands
        
    def describe_current_room(self, proselen=60):
        descr = ai_interface.describe_room(descr=self.context(), proselen=proselen)
        print(fill(descr))
        
    def interact(self):
        action = input(">> ")
        
        commands = ai_interface.command(descr=self.context(), prompt=action, defs="\n----\n".join(self.defs.values()))
        logging.debug("ran command: {commands}")
        
        for command in commands.split(";;"):
            #assert (command[0], command[-1]) == ("`", "`"), f"{command} has invalid format"
            to_exec = command.replace("`", "").strip()
            c, *args = to_exec.split(" ")
            
            if c == 'DEFAULT':
                print("ERROR: ", *args)
                break
            
            context_before = self.context()
            
            try:
            
                result = self.funcs[c](self, *args)
            except AssertionError as ex:
                result = "Failure: " + str(ex)
            
            context_after = {k:v for k,v in self.context().items() if v != context_before.get(k, 0)}
            
            proselen = 50 if c in ('LOOK_AT', 'LOOK_AT_CONTENT') else 20
            
            print(fill(ai_interface.execute(before=context_before, after=context_after, defs=self.defs[c], command=to_exec, res=result, proselen=proselen, prompt=action)))
            
    @classmethod
    def register(cls, func):
        name = func.__name__.upper()
        options = " ".join(f"[{x.upper()}]" for x in inspect.getfullargspec(func)[0][1:])
        descr = func.__doc__
        definition = f"`{name} {options} `\n{descr}"
        cls.funcs[name] = func
        cls.defs[name] = definition
        


        



    
        
        
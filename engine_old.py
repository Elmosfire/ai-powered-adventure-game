from dataclasses import dataclass
from typing import List, Dict
import yaml
from interface import interface
from random import choice, randrange

with open("rng-table.yml") as file:
    RandProps = yaml.safe_load(file)
    
def random_word(key):
    return choice(RandProps[key])

@dataclass
class Entity:
    id_: str
    description: str
    visual_properties: Dict
    contents: List
    open: bool = True
    locked: bool = False
    equipment_type: str = ""
    can_be_picked_up: bool = False
    map_transfer: str = None
    
    def find_entity(self, id_):
        if self.id_ == id_:
            return self
        if self.open:
            for x in self.contents:
                q = x.find_entity(id_)
                if q:
                    return q
        return None
    
    def as_json_for_interface(self):
        return dict(
            description=self.description,
            visual_properties=self.visual_properties,
            contents={x.id_: x.as_json_for_interface() for x in self.contents}
        )
        
@dataclass
class Container(Entity):
    open: bool = False
    def as_json_for_interface(self):
        return dict(
            description=self.description,
            visual_properties=self.visual_properties,
            open=self.open,
            contents={x.id_: x.as_json_for_interface() for x in self.contents} if self.open else {}
        )
    def open(self):
        self.open = True

    
def entity_from_dict(id_, config):
    
    config["id_"] = id_
    config["contents"] = [entity_from_dict(k, v) for k, v in config.get("contents", {}).items()]
    
    return Entity(**config)

def find_entity_in_list(id_, lst):
    for x in lst:
        q = x.find_entity(id_)
        if q:
            return q
    return None

@dataclass
class Location:
    id_: str
    description: str
    visual_properties: dict
    contents: List[Entity]
    
    def as_json_for_interface(self):
        return dict(
            description=self.description,
            visual_properties=self.visual_properties,
            contents={x.id_: x.as_json_for_interface() for x in self.contents}
        )

def room_from_dict(id_, config):
    config["id_"] = id_
    config["contents"] = [entity_from_dict(k, v) for k, v in config.get("contents", {}).items()]
    return Location(**config)



        
def random_jewel():
    descr = random_word("jewel")
    mat = random_word("material-jewel")
    return Entity("$" + descr + hex(randrange(16**16)),description=descr, visual_properties=dict(material=mat), contents=[])


def random_lootable():
    descr = random_word("lootable")
    mat = random_word("material-furniture")
    return Container("$" + descr + hex(randrange(16**16)),description=descr, visual_properties=dict(material=mat), contents=[random_jewel() for _ in range(5)], open=False)

def generate_room():
    mat = random_word("material-furniture")
    return Location("$room" + hex(randrange(16**16)),description="room", visual_properties=dict(walls=dict(material=mat)), contents=[random_lootable()])
    
    


with open("adventure.yaml") as file:
    ROOMS = [generate_room()]
    
print(ROOMS)
        
@dataclass
class Player:
    location: Location
    equipment: dict
    right_hand: Entity = None
    left_hand: Entity = None
    
    
    
    def interact(self):
        descr_json =self.location.as_json_for_interface()
        print(descr_json)
        descr = interface.describe_room(descr=descr_json)
        print(descr)
        
        action = input(">>")
        
        command = interface.command(descr=descr_json, prompt=action)
        
        print("???", command)
        
        assert (command[0], command[-1]) == ("`", "`"), f"{command} has invalid format"
        
        c, *args = command[1:-1].split(" ")
        
        if c == "LOOK_AT":
            obj, = args
            obj = find_entity_in_list(obj, self.location.contents)
            descr_json = obj.as_json_for_interface()
            print(interface.describe_object(descr=descr_json))
            
        
            
        
        

Player(ROOMS[0], {}).interact()
    
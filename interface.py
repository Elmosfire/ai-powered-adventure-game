from openai import OpenAI
from pathlib import Path
from functools import partial
import logging
import tiktoken
import shelve

    

MODEL = "gpt-4-1106-preview"


encoding = tiktoken.encoding_for_model(MODEL)

client = OpenAI()


def ask_prompt(prompt_file,** options):
    with (Path("prompts") / prompt_file).with_suffix(".txt").open() as file:
        prompt = file.read()
        
    setting = prompt_file +":\n" + '\n'.join(f"{k}= {v}" for k,v in options.items())
    
    logging.debug(f"request: {setting}")
    with shelve.open('cache') as cache:
        if setting not in cache:
        
            for k,v in options.items():
                prompt = prompt.replace(f"<<{k}>>", str(v))
                
            num_tokens = len(encoding.encode(prompt))
            logging.debug("send: %s %s", num_tokens, "tokens")
            res = client.chat.completions.create(
                model=MODEL,
                messages=[{"role": "user", "content": prompt}],
                max_tokens=200
            )
            cache[setting] = res.choices[0].message.content
            num_tokens = len(encoding.encode(cache[setting]))
            logging.debug("recieved: %s %s", num_tokens, "tokens")
            
        logging.debug(f"reply: \n{cache[setting]}")

        return cache[setting]

class InterfaceWrapper:
    def __getattr__(self, attr):
        return partial(ask_prompt, attr)
    
    
ai_interface = InterfaceWrapper()